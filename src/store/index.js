import { createStore, applyMiddleware } from 'redux'
import reducers from './reducers'
import logger from 'redux-logger'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['token', 'accountInfo']
}
const persistedReducer = persistReducer(persistConfig, reducers)

let middleware = applyMiddleware(logger)

export let store = createStore(persistedReducer, middleware)
export let persistor = persistStore(store)
import * as types from './types'

const initialState = {
  token: {
    access_token: null,
    expires_in: null,
    refresh_token: null,
    token_type: 'Bearer'
  },
  accountInfo: {
    email: '',
    name: '',
    address: '',
    phone: '',
    id: '',
    partner_code: '',
    help_info: '',
    agreement_info: ''
  }
}

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_TOKEN:
      return {
        ...state,
        token: {
          ...state.token,
          ...action.token
        }
      }

    case types.SET_USER_INFO:
      return {
        ...state,
        accountInfo: {
          ...state.accountInfo,
          ...action.account
        }
      }

    case types.CLEAR_TOKEN:
      return {
        ...initialState
      }

    default:
      return state
  }
}

export default reducers
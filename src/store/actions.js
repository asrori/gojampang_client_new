import * as types from './types'

export const setToken = (token) => {
  return {
    type: types.SET_TOKEN,
    token
  }
}

export const setUserInfo = (account) => {
  return {
    type: types.SET_USER_INFO,
    account
  }
}

export const clearToken = () => {
  return {
    type: types.CLEAR_TOKEN
  }
}
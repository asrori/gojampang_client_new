export const SET_TOKEN = 'token/SET'
export const SET_USER_INFO = 'token/USER_INFO'
export const CLEAR_TOKEN = 'token/CLEAR'
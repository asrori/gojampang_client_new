import React from 'react';
import { createStackNavigator, createDrawerNavigator } from 'react-navigation'
import Sidebar from './screens/Sidebar/SideBar'
import EntryOrder from './screens/EntryOrder';
import DetailsOrder from './screens/EntryOrder/DetailsOrder';
import ConfirmedOrder from './screens/ConfirmedOrder';
import RejectedOrder from './screens/RejectedOrder';
import HistoryOrder from './screens/HistoryOrder';
import Help from './screens/Help';
import Agreement from './screens/Agreement';
import ConfirmedForm from './screens/ConfirmedOrder/ConfirmedForm';
import DetailsRejected from './screens/RejectedOrder/DetailsRejected'
import DetailsHistory from './screens/HistoryOrder/DetailsHistory'
import Login from './screens/auth/Login'
import ForgetPassword from './screens/auth/ForgetPassword'
import VerifyForgetPassword from './screens/auth/VerifyForgetPassword'

// pesanan masuk
const EntryOrderStack = createStackNavigator(
  {
    EntryOrder: { screen: EntryOrder },
    DetailsOrder: { screen: DetailsOrder }
  },
  {
    initialRouteName: 'EntryOrder',
    headerMode: 'none'
  }
)

EntryOrderStack.navigationOptions = ({ navigation }) => {
  let drawerLockMode = 'unlocked';
  if (navigation.state.index > 0) {
    drawerLockMode = 'locked-closed';
  }
  return {
    drawerLockMode,
  }
}

// pesanan dikonfirm
const ConfirmedOrderStack = createStackNavigator(
  {
    ConfirmedOrder: { screen: ConfirmedOrder },
    ConfirmedForm: { screen: ConfirmedForm }
  },
  {
    initialRouteName: 'ConfirmedOrder',
    headerMode: 'none'
  }
)

ConfirmedOrderStack.navigationOptions = ({ navigation }) => {
  let drawerLockMode = 'unlocked';
  if (navigation.state.index > 0) {
    drawerLockMode = 'locked-closed';
  }
  return {
    drawerLockMode,
  }
}

// pesanan ditolak
const RejectedOrderStack = createStackNavigator(
  {
    RejectedOrder: { screen: RejectedOrder },
    DetailsRejected: { screen: DetailsRejected }
  },
  {
    initialRouteName: 'RejectedOrder',
    headerMode: 'none'
  }
)

RejectedOrderStack.navigationOptions = ({ navigation }) => {
  let drawerLockMode = 'unlocked';
  if (navigation.state.index > 0) {
    drawerLockMode = 'locked-closed';
  }
  return {
    drawerLockMode,
  }
}

// history pesanan
const HistoryOrderStack = createStackNavigator(
  {
    HistoryOrder: { screen: HistoryOrder },
    DetailsHistory: { screen: DetailsHistory }
  },
  {
    initialRouteName: 'HistoryOrder',
    headerMode: 'none'
  }
)

HistoryOrderStack.navigationOptions = ({ navigation }) => {
  let drawerLockMode = 'unlocked';
  if (navigation.state.index > 0) {
    drawerLockMode = 'locked-closed';
  }
  return {
    drawerLockMode,
  }
}

// bantuan
const HelpStack = createStackNavigator(
  {
    Help: { screen: Help }
  },
  {
    initialRouteName: 'Help',
    headerMode: 'none'
  }
)

// perjanjian
const AgreementStack = createStackNavigator(
  {
    Agreement: { screen: Agreement }
  },
  {
    initialRouteName: 'Agreement',
    headerMode: 'none'
  }
)

const Navigator = (initialRouteName) => {
  return createStackNavigator (
    {
      Login: Login,
      ForgetPassword : ForgetPassword,
      VerifyForgetPassword : VerifyForgetPassword,
      MainScreen: createDrawerNavigator(
        {
          EntryOrders: EntryOrderStack,
          ConfirmedOrders: ConfirmedOrderStack,
          RejectedOrders: RejectedOrderStack,
          HistoryOrders: HistoryOrderStack,
          Helps: HelpStack,
          Agreements: AgreementStack
        },
        {
          initialRouteName: 'EntryOrders',
          contentComponent: props => <Sidebar {...props} />
        }
      )
    },
    {
      initialRouteName,
      headerMode: 'none'
    }
  )
}

export default Navigator

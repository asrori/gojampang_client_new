import React, { Component } from 'react';
import { View, RefreshControl, ScrollView } from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  List,
  ListItem,
  Spinner
} from 'native-base';
import fetch from '../../services/fetch'
import { historyOrders } from '../../config/api'

class EntryOrder extends Component {
  constructor (props) {
    super(props)
    this.initialState = {
      loading: false,
      data: [],
      page: 1
    }
    this.state = this.initialState
  }

  componentDidMount() {
    this.getListOrder()
  }

  getListOrder(page = 1) {
    this.setState({ loading: true })
    const params = {
      page: page
    }
    fetch.get(historyOrders, {params})
      .then(res => {
        const { data } = res.data.data
        this.setState({
          data: page === 1 ? data : [...this.state.data, ...data],
          loading: false
        })
      })
      .catch(err => {
        this.setState({ loading: false })
        console.log(err)
      })
  }

  handleRefresh () {
    this.setState({
      page: 1
    }, () => {
      this.getListOrder(this.state.page)
    })
  }

  handleLoadMore () {
    this.setState({
      page: this.state.page + 1
    }, () => {
      this.getListOrder(this.state.page)
    })
  }

  render() {
    const { data } = this.state
    return (
      <Container style={{ flex: 1}}>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.toggleDrawer()}
            >
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Riwayat Pesanan</Title>
          </Body>
          <Right />
        </Header>

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.loading}
              onRefresh={this.handleRefresh.bind(this)}
            />
          }
        >
          <List>
            {data.length > 0 ?
              data.map((item, i) => {
                return (
                  <ListItem
                    key={i}
                    avatar
                    button
                    onPress={() => this.props.navigation.navigate('DetailsHistory', {venueId: item.id})}
                  >
                    <Left style={{ width: 35 }}>
                      <Text note style={{ textAlign: 'center' }}>{item.order_date}</Text>
                    </Left>
                    <Body>
                      <Text>{item.customer.name} / {item.customer.phone}</Text>
                      <Text note>Pesanan baru dengan no. {item.order_number} klik untuk melihat detail selengkapnya . .</Text>
                    </Body>
                    <Right>
                      <Text note>{item.order_time}</Text>
                    </Right>
                  </ListItem>
                )
              }) : (<View style={{flex: 1, justifyContent: 'center',  alignItems: 'center', marginTop: 10}}><Text>No Data Available</Text></View>)
            }
            {!this.state.loading && this.state.data.length > 0 && (
            <View style={{flex: 1}}>
              <Button
                onPress={this.handleLoadMore.bind(this)}
                transparent
                success
                block
                style={{ padding: 10, paddingLeft: 20, paddingRight: 20 }}>
                <Text style={{ fontSize: 13}}>
                  LOAD MORE
                </Text>
              </Button>
            </View>)}
          </List>
        </ScrollView>
      </Container>
    );
  }
}

export default EntryOrder;

import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  List,
  ListItem
} from 'native-base';
import { connect } from "react-redux";

class Agreement extends Component {
  render() {
    return (
      <Container style={{ flex: 1 }}>
        <Header>
          <Left style={{flex: 0, paddingLeft: 6, width: 62}}>
            <Button transparent onPress={() => this.props.navigation.toggleDrawer()}>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body >
            <Title>Perjanjian Kemitraan</Title>
          </Body>
        </Header>

        <Content>
          <View style={{flex: 1, marginHorizontal: 10, marginVertical: 10}}>
            <Text>{this.props.account.agreement_info}</Text>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  account: state.accountInfo
})

export default connect(mapStateToProps, null)(Agreement)

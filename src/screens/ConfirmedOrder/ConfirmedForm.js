import React, { Component } from 'react';
import { View, Text, TextInput, Alert } from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Form,
  Item,
  Input,
  Label,
  Picker,
  Textarea,
  Radio,
  ListItem
} from 'native-base';
import {
  StackActions,
  NavigationActions
} from 'react-navigation'
import { Col, Row, Grid } from 'react-native-easy-grid';
import fetch from '../../services/fetch';
import { orders } from '../../config/api';
import Spinner from 'react-native-loading-spinner-overlay'

class ConfirmedForm extends Component {
  constructor(props) {
    super(props)
    this.initialState = {
      loading: false,
      form: {
        status_payment: '',
        payment_method: '',
        additional_notes: ''
      },
      info: {
        type: '',
        category: '',
        product_code: '',
        order_date: '',
        reserved_date: '',
        amount_order: 0,
        customer: {
          name: '',
          phone: ''
        },
        notes: ''
      },
      isUpdate: false
    }
    this.state = this.initialState
  }

  componentDidMount() {
    this.getOrderDetail()
  }

  getOrderDetail() {
    const { venueId } = this.props.navigation.state.params
    this.setState({ loading: true })
    fetch.get(`${orders}/venue/${venueId}`)
      .then(res => {
        const { data } = res.data
        this.setState({
          info: data,
          form: {
            ...this.state.form,
            status_payment: data.status_payment,
            payment_method: data.payment_method,
            additional_notes: data.additional_notes
          },
          isUpdate: data.payment_method !== null ? true : false,
          loading: false
        })
      })
      .catch(err => {
        this.setState({ loading: false })
        console.log(err)
      })
  }

  backHandler () {
    Alert.alert(
      'Success',
      'Data berhasil diperbarui',
      [{
        text: 'OK',
        onPress: () =>
        this.props.navigation.dispatch(
          StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'ConfirmedOrder' })]
          })
        )
      }],
      { cancelable: false }
    )
  }

  doUpdate () {
    const { id } = this.state.info
    this.setState({loading: true})
    fetch.put(`${orders}/venue/${id}`, this.state.form)
      .then(res => {
        const { data } = res.data
        this.setState({
          form: {
            ...this.state.form,
            status_payment: 'paid',
            payment_method: 'transfer',
            additional_notes: ''
          },
          loading: false
        })
        this.backHandler()
      })
      .catch(err => {
        this.setState({loading: false})
        Alert.alert('Ops Terjadi Kesalahan')
        console.log(err)
      })
  }

  render() {
    let {form, info, isUpdate} = this.state
    let editable = false
    return (
      <Container style={{ flex: 1 }}>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate('ConfirmedOrder')}
            >
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body >
            <Title>Detail</Title>
          </Body>
          <Right style={{ width: 30 }} />
        </Header>

        <Content>
          <Spinner visible={this.state.loading} />
          <Form>
            <Grid style={{marginVertical: 10, marginHorizontal: 10}}>
              <Row>
                <Col>
                  <Label>Jenis</Label>
                  <Item style={{marginLeft: 0, marginRight: 4}}>
                    <Input value={info.type} editable={editable} />
                  </Item>
                </Col>
                <Col>
                  <Label>Kategori</Label>
                  <Item style={{marginLeft: 0}}>
                    <Input value={info.category} editable={editable} />
                  </Item>
                </Col>
              </Row>

              {/* nama pemesan */}
              <Row style={{marginTop: 5}}>
                <Col style={{ width: 160, justifyContent: 'center' }} >
                  <Text>Nama Pemesan</Text>
                </Col>
                <Col>
                  <Item inlineLabel><Input value={info.customer.name} editable={editable} /></Item>
                </Col>
              </Row>

              {/* telepon */}
              <Row style={{marginTop: 5}}>
                <Col style={{ width: 160, justifyContent: 'center' }} >
                  <Text>Telp / HP</Text>
                </Col>
                <Col>
                  <Item inlineLabel><Input value={info.customer.phone} editable={editable} /></Item>
                </Col>
              </Row>

              {/* kode product */}
              <Row style={{marginTop: 5}}>
                <Col style={{ width: 160, justifyContent: 'center' }} >
                  <Text>Kode Product</Text>
                </Col>
                <Col>
                  <Item inlineLabel><Input value={info.product_code} editable={editable} /></Item>
                </Col>
              </Row>

              {/* tanggal pesan */}
              <Row style={{marginTop: 5}}>
                <Col style={{ width: 160, justifyContent: 'center' }} >
                  <Text>Tanggal Pesan</Text>
                </Col>
                <Col>
                  <Item inlineLabel><Input value={info.order_date} editable={editable} /></Item>
                </Col>
              </Row>

              {/* untuk tangal */}
              <Row style={{marginTop: 5}}>
                <Col style={{ width: 160, justifyContent: 'center' }} >
                  <Text>Untuk Tanggal</Text>
                </Col>
                <Col>
                  <Item inlineLabel><Input value={info.reserved_date} editable={editable} /></Item>
                </Col>
              </Row>

              {/* jumlah pesanan */}
              <Row style={{marginTop: 5}}>
                <Col style={{ width: 160, justifyContent: 'center' }} >
                  <Text>Jumlah Pesanan</Text>
                </Col>
                <Col>
                  <Item inlineLabel>
                    <TextInput
                      style={{fontWeight: 'bold', color: 'black'}}
                      keyboardType='numeric'
                      value={`${info.amount_order}`}
                      editable={editable}
                    />
                  </Item>
                </Col>
              </Row>

              {/* status pembayaran */}
              <Row style={{marginTop: 15}}>
                <Text>Status Pembayaran</Text>
              </Row>
              <Row>
                <Col>
                  <ListItem>
                    <Radio
                      onPress={() => this.setState({
                        form: { ...form, status_payment: 'paid' }
                      })}
                      selected={form.status_payment == 'paid' }
                    />
                    <Text style={{marginLeft: 10}}>Lunas</Text>
                  </ListItem>
                </Col>
                <Col>
                  <ListItem>
                    <Radio
                      onPress={() => this.setState({
                        form: { ...form, status_payment: 'booked' }
                      })}
                      selected={form.status_payment == 'booked' }
                    />
                    <Text style={{marginLeft: 10}}>Booked</Text>
                  </ListItem>
                </Col>
              </Row>

              {/* sistem pembayaran */}
              <Row style={{marginTop: 15}}>
                <Text>Sistem Pembayaran</Text>
              </Row>
              <Row>
                <Col>
                  <ListItem>
                    <Radio
                      onPress={() => this.setState({
                        form: { ...form, payment_method: 'transfer' }
                      })}
                      selected={form.payment_method == 'transfer' }
                    />
                    <Text style={{marginLeft: 10}}>Transfer</Text>
                  </ListItem>
                </Col>
                <Col>
                  <ListItem>
                    <Radio
                      onPress={() => this.setState({
                        form: { ...form, payment_method: 'ots' }
                      })}
                      selected={form.payment_method == 'ots' }
                    />
                    <Text style={{marginLeft: 10}}>On The Spot</Text>
                  </ListItem>
                </Col>
              </Row>
               {/* catatan user */}
               <Row style={{marginTop: 5}}>
                <Col style={{ width: 160, justifyContent: 'center' }} >
                  <Text>Catatan dari user</Text>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Textarea
                    value={info.notes}
                    rowSpan={4}
                    bordered
                    editable={false}
                    disabled={true}
                    placeholder="Catatan User"
                  />
                </Col>
              </Row>
              <Row style={{marginTop: 15}}>
                <Col>
                  <Textarea
                    value={form.additional_notes}
                    onChangeText={additional_notes => this.setState({ form: { ...form, additional_notes }})}
                    rowSpan={5}
                    bordered
                    disabled={isUpdate}
                    placeholder="Tambah Catatan"
                  />
                </Col>
              </Row>
              <Row style={{marginTop: 5}}>
                <Text style={{color: 'red', fontSize: 11,}}>* Feedback</Text>
              </Row>
              {!isUpdate && <Row style={{marginTop: 5}}>
                <Col>
                  <Button block disabled={!form.additional_notes || !form.payment_method || !form.status_payment} onPress={() => {this.doUpdate()}}>
                    <Text style={{color: '#fff'}}>Perbarui</Text>
                  </Button>
                </Col>
              </Row>}
            </Grid>
          </Form>
        </Content>
      </Container>
    );
  }
}

export default ConfirmedForm;

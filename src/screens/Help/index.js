import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  List,
  ListItem
} from 'native-base';
import { connect } from "react-redux";

class Help extends Component {
  render() {
    return (
      <Container style={{ flex: 1 }}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.toggleDrawer()}>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body >
            <Title>Bantuan</Title>
          </Body>
          <Right style={{ width: 30 }} />
        </Header>

        <Content>
          <View style={{flex: 1, marginHorizontal: 10, marginVertical: 10}}>
            <Text>{this.props.account.help_info}</Text>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  account: state.accountInfo
})

export default connect(mapStateToProps, null)(Help)
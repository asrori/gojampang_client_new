import React, { Component } from 'react';
import { View, Text, Image, TextInput, Alert } from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Form,
  Item,
  Input,
  Label,
  Picker,
  Textarea
} from 'native-base';
import {
  StackActions,
  NavigationActions
} from 'react-navigation'
import Spinner from 'react-native-loading-spinner-overlay';
import { Col, Row, Grid } from 'react-native-easy-grid';
import fetch from '../../services/fetch';
import { orders } from '../../config/api';

class DetailsOrder extends Component {
  constructor(props) {
    super(props)
    this.initialState = {
      loading: false,
      form: {
        approval_notes: ''
      },
      info: {
        type: '',
        category: '',
        product_code: '',
        order_date: '',
        reserved_date: '',
        amount_order: 0,
        customer: {
          name: '',
          phone: ''
        },
        notes: ''
      }
    }
    this.state = this.initialState
  }

  componentDidMount() {
    this.getOrderDetail()
  }

  getOrderDetail() {
    const { venueId } = this.props.navigation.state.params
    this.setState({ loading: true })
    fetch.get(`${orders}/venue/${venueId}`)
      .then(res => {
        const { data } = res.data
        this.setState({ info: data, loading: false })
      })
      .catch(err => {
        this.setState({ loading: false })
        console.log(err)
      })
  }

  backHandler () {
    Alert.alert(
      'Success',
      'Data berhasil diperbarui',
      [{
        text: 'OK',
        onPress: () =>
        this.props.navigation.dispatch(
          StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'EntryOrder' })]
          })
        )
      }],
      { cancelable: false }
    )
  }

  doConfirm () {
    const { id } = this.state.info
    this.setState({loading: true})
    fetch.put(`${orders}/venue/${id}/confirm`, this.state.form)
      .then(res => {
        const { data } = res.data
        this.setState({
          form: {...this.state.form, approval_notes: ''},
          loading: false
        })
        this.backHandler()
      })
      .catch(err => {
        this.setState({loading: false})
        Alert.alert('Ops Terjadi Kesalahan')
        console.log(err)
      })
  }

  doReject () {
    const { id } = this.state.info
    this.setState({loading: true})
    fetch.put(`${orders}/venue/${id}/reject`, this.state.form)
      .then(res => {
        const { data } = res.data
        this.setState({
          form: {...this.state.form, approval_notes: ''},
          loading: false
        })
        this.backHandler()
      })
      .catch(err => {
        this.setState({loading: false})
        Alert.alert('Ops Terjadi Kesalahan')
        console.log(err)
      })
  }

  render() {
    let {form, info} = this.state
    let editable = false
    return (
      <Container style={{ flex: 1 }}>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate('EntryOrder')}
            >
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body >
            <Title>Detail</Title>
          </Body>
          <Right style={{ width: 30 }} />
        </Header>

        <Content>
          <Spinner visible={this.state.loading} />
          <Form>
            <Grid style={{marginVertical: 10, marginHorizontal: 10}}>
              <Row>
                <Col>
                  <Label>Jenis</Label>
                  <Item style={{marginLeft: 0, marginRight: 4}}>
                    <Input value={info.type} editable={editable} />
                  </Item>
                </Col>
                <Col>
                  <Label>Kategori</Label>
                  <Item style={{marginLeft: 0}}>
                    <Input value={info.category} editable={editable} />
                  </Item>
                </Col>
              </Row>

              {/* nama pemesan */}
              <Row style={{marginTop: 5}}>
                <Col style={{ width: 160, justifyContent: 'center' }} >
                  <Text>Nama Pemesan</Text>
                </Col>
                <Col>
                  <Item inlineLabel><Input value={info.customer.name} editable={editable} /></Item>
                </Col>
              </Row>

              {/* telepon */}
              <Row style={{marginTop: 5}}>
                <Col style={{ width: 160, justifyContent: 'center' }} >
                  <Text>Telp / HP</Text>
                </Col>
                <Col>
                  <Item inlineLabel><Input value={info.customer.phone} editable={editable} /></Item>
                </Col>
              </Row>

              {/* kode product */}
              <Row style={{marginTop: 5}}>
                <Col style={{ width: 160, justifyContent: 'center' }} >
                  <Text>Kode Product</Text>
                </Col>
                <Col>
                  <Item inlineLabel><Input value={info.product_code} editable={editable} /></Item>
                </Col>
              </Row>

              {/* tanggal pesan */}
              <Row style={{marginTop: 5}}>
                <Col style={{ width: 160, justifyContent: 'center' }} >
                  <Text>Tanggal Pesan</Text>
                </Col>
                <Col>
                  <Item inlineLabel><Input value={info.order_date} editable={editable} /></Item>
                </Col>
              </Row>

              {/* untuk tangal */}
              <Row style={{marginTop: 5}}>
                <Col style={{ width: 160, justifyContent: 'center' }} >
                  <Text>Untuk Tanggal</Text>
                </Col>
                <Col>
                  <Item inlineLabel><Input value={info.reserved_date} editable={editable} /></Item>
                </Col>
              </Row>

              {/* jumlah pesanan */}
              <Row style={{marginTop: 5}}>
                <Col style={{ width: 160, justifyContent: 'center' }} >
                  <Text>Jumlah Pesanan</Text>
                </Col>
                <Col>
                  <Item inlineLabel>
                    <TextInput
                      style={{fontWeight: 'bold', color: 'black'}}
                      keyboardType='numeric'
                      value={`${info.amount_order}`}
                      editable={editable}
                    />
                  </Item>
                </Col>
              </Row>
              {/* catatan user */}
              <Row style={{marginTop: 5}}>
                <Col style={{ width: 160, justifyContent: 'center' }} >
                  <Text>Catatan dari user</Text>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Textarea
                    value={info.notes}
                    rowSpan={4}
                    bordered
                    disabled={true}
                    placeholder="Catatan User"
                  />
                </Col>
              </Row>

              {/* catatan */}
              <Row style={{marginTop: 15}}>
                <Col>
                  <Textarea
                    value={form.approval_notes}
                    onChangeText={approval_notes => this.setState({ form: { ...form, approval_notes }})}
                    rowSpan={5}
                    bordered
                    placeholder="Tambah Catatan"
                  />
                </Col>
              </Row>
              <Row style={{marginTop: 5}}>
                <Text style={{color: 'red', fontSize: 11,}}>* Jika Stock Habis Catatan Wajib Diisi</Text>
              </Row>
              <Row style={{marginTop: 5}}>
                <Col>
                  <Button block onPress={() => {this.doConfirm()}}>
                    <Text style={{color: '#fff'}}>KONFIRM</Text>
                  </Button>
                </Col>
              </Row>
              <Row style={{marginTop: 5}}>
                <Col>
                  <Button block disabled={!form.approval_notes} onPress={() => {this.doReject()}}>
                    <Text style={{color: '#fff'}}>STOK HABIS</Text>
                  </Button>
                </Col>
              </Row>
            </Grid>
          </Form>
        </Content>
      </Container>
    );
  }
}

export default DetailsOrder;

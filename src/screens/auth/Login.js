import React from 'react'
import { StyleSheet, View, TouchableOpacity, ScrollView, Image } from 'react-native'
import {
  Container,
  Form,
  Item,
  Label,
  Input,
  Button,
  Text,
  Toast,
  Icon,
  ListItem,
  CheckBox,
  Body,
  Left
} from 'native-base'
import {
  StackActions,
  NavigationActions
} from 'react-navigation'
import Config from 'react-native-config'
import fetch from '../../services/fetch'
import { login, accountInfo } from '../../config/api'
import Spinner from 'react-native-loading-spinner-overlay'
import { setToken, setUserInfo } from '../../store/actions'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

class Login extends React.Component {
  constructor(props) {
    super(props)
    this.initialState = {
      loading: false,
      showPassword: true,
      form: {
        username: '',
        password: ''
      }
    }
    this.state = this.initialState
  }

  handleLogin = () => {
    this.setState({ loading: true })
    const params = {
      grant_type : Config.GRANT_TYPE,
      client_id : Config.CLIENT_ID,
      client_secret : Config.CLIENT_SECRET,
      username : this.state.form.username,
      password : this.state.form.password,
      scope : ""
    }
    fetch.post(login, params)
      .then(res => {
        const {data} = res
        this.props.setToken(data)
        this.setState({ loading: false })
        fetch.defaults.headers.common['Authorization'] = `Bearer ${data.access_token}`

        fetch.get(accountInfo)
          .then(res => {
            const { data } = res.data
            this.props.setUserInfo(data.account)
          })
          .catch(err => {
            console.log(err.response)
            let message = 'Ops terjadi kesalahan'
            Toast.show({
              text: message,
              duration: 3000
            })
          })

        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'MainScreen' })]
        })
        this.props.navigation.dispatch(resetAction)
      })
      .catch(err => {
        this.setState({ loading: false })
        let message = 'Username / Password Salah'
        if (err.response && err.response.data.errors) {
          message = err.response.data.errors[0]
        }
        Toast.show({
          text: message,
          duration: 3000
        })
      })
  }

  togglePassword () {
    this.setState({showPassword: !this.state.showPassword})
  }

  render() {
    let {form, showPassword} = this.state
    let disabled = !form.username || !form.password
    return (
      <Container>
        {/* <ScrollView> */}
          <View style={{ flex:1, justifyContent: 'center'}}>
            <Spinner visible={this.state.loading} />
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Image
                source={require('../../assets/logo-jampang.png')}
                style={{width: 150, height: 150}}
                resizeMode="contain"
              />
            </View>
            <Form style={{marginHorizontal: 5}}>
              <Item rounded style={style.fieldItem}>
                <Icon name="person" style={style.fieldIcon}></Icon>
                <Input
                  placeholder="Username"
                  keyboardType="email-address"
                  returnKeyType="next"
                  autoCapitalize="none"
                  autoCorrect={false}
                  value={form.username}
                  onChangeText={username => this.setState({ form: { ...form, username }})}
                  onSubmitEditing={(event) => {
                    this._password._root.focus();
                  }}
                  disabled={this.state.loading}
                />
              </Item>
              <Item rounded style={style.fieldItem}>
                <Icon name="unlock" style={style.fieldIcon}></Icon>
                <Input
                  ref={(c) => this._password = c}
                  placeholder="Password"
                  secureTextEntry={showPassword}
                  value={form.password}
                  onChangeText={password => this.setState({ form: { ...form, password }})}
                  onSubmitEditing={this.handleLogin}
                />
                <TouchableOpacity
                  style={{ paddingRight: 10 }}
                  onPress={() => this.togglePassword()}
                >
                  <Icon name={showPassword ? 'eye' : 'eye-off'} style={style.fieldIcon}></Icon>
                </TouchableOpacity>
              </Item>
            </Form>
            <View style={{ padding: 10, marginTop: 10 }}>
              <Button loading block onPress={this.handleLogin} disabled={disabled}>
                <Text>Masuk</Text>
              </Button>
              <Button transparent block style={{ marginVertical: 10 }}
              onPress={() => this.props.navigation.navigate('ForgetPassword')}>
              <Text>Lupa Password</Text>
            </Button>
            </View>
          </View>
        {/* </ScrollView> */}
      </Container>
    )
  }
}

const style = StyleSheet.create({
  fieldIcon: {
    color: 'grey'
  },
  fieldItem: {
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
    borderColor: 'rgba(68, 68, 68, 0.2)',
    marginTop: 10,
    paddingLeft: 10
  }
})

const mapDispatchToProps = (dispatch) => ({
  setToken: bindActionCreators(setToken, dispatch),
  setUserInfo: bindActionCreators(setUserInfo, dispatch)
})

export default connect(null, mapDispatchToProps)(Login)
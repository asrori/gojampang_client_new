import React from 'react'
import {
  Container,
  Form,
  Item,
  Label,
  Input,
  Button,
  Text,
  View,
  Toast,
  Header,
  Body,
  Title,
  Left,
  Right,
  Icon
} from 'native-base'
import {
  StackActions,
  NavigationActions
} from 'react-navigation'
import fetch from '../../services/fetch'
import { forgotPassword } from '../../config/api'
import Spinner from 'react-native-loading-spinner-overlay'

class VerifyForgetPassword extends React.Component {
  constructor(props) {
    super(props)
    this.initialState = {
      loading: false,
      phone: ''
    }
    this.state= this.initialState
  }

  submitVerifyForgetPassword = () => {
    this.setState({ loading: true })
    const params = {
      phone: this.state.phone
    }
    fetch.post(forgotPassword, params)
      .then(res => {
        console.log(res)
        this.setState({ loading: false })
        this.props.navigation.navigate('VerifyForgetPassword', params)
      })
      .catch(err => {
        console.log(err, err.response)
        this.setState({ loading: false })
        Toast.show({
          text: err.response.data.msg,
          duration: 3000,
          buttonText: 'Ok'
        })
      })
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="close" />
            </Button>
          </Left>
          <Body>
            <Title>Reset Password</Title>
          </Body>
          <Right />
        </Header>
        <View style={{ flex:1, justifyContent: 'center'}}>
          <Form>
            <Item stackedLabel>
              <Label>Masukkan No. Telepon / Email</Label>
              <Input
                value={this.state.phone}
                onChangeText={phone => this.setState({phone})}
                keyboardType="phone-pad"
              />
            </Item>
          </Form>
          <View style={{ padding: 10, marginTop: 10 }}>
            <Button block onPress={this.submitVerifyForgetPassword}
              disabled={!Boolean(this.state.phone)}
            >
              <Text>Reset Password</Text>
            </Button>
          </View>
        </View>
        <View style={{ padding: 15 }}>
          <Text style={{ color: '#616161' }}>Kami akan mengirimkan kode verifikasi ke nomor anda.</Text>
        </View>
        <Spinner visible={this.state.loading} />
      </Container>
    )
  }
}

export default VerifyForgetPassword
import React from 'react'
import { TouchableOpacity, BackHandler } from 'react-native'
import {
  Container,
  Content,
  View,
  Text,
  Toast,
  Item,
  Label,
  Input,
  Button
} from 'native-base'
import Config from 'react-native-config'
import fetch from '../../services/fetch'
import { customer, forgotPassword, resetPassword } from '../../config/api'
import Spinner from 'react-native-loading-spinner-overlay'
import { StackActions, NavigationActions } from 'react-navigation'
import { setToken } from '../../store/actions'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

class VerifyForgetPassword extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      timer: 30,
      enableResend: true,
      form: {
        phone: this.props.navigation.getParam('phone', ''),
        code: '',
        password: '',
        password_confirmation: ''
      },
      errors: []
    }
    this.countdown = null
    console.log('terima', this.state.phone)
  }

  componentDidMount() {
    this.runCountdown()
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove()
    if (this.countdown) {
      clearInterval(this.countdown)
    }
  }

  runCountdown() {
    this.setState({ timer: 30, enableResend: false })
    this.countdown = setInterval(() => {
      if (this.state.timer > 1) {
        this.setState({ timer: this.state.timer - 1 })
      } else {
        this.setState({ enableResend: true })
        clearInterval(this.countdown)
      }
    }, 1000)
  }

  resendCode() {
    this.setState({ loading: true })
    const { phone } = this.state.form
    fetch.post(forgotPassword, { phone })
      .then(res => {
        Toast.show({
          text: "Kode verifikasi terkirim",
          duration: 3000
        })
        this.setState({ loading: false })
        this.runCountdown()
      })
      .catch(err => {
        Toast.show({
          text: "Gagal mengirim kode verifikasi",
          duration: 3000
        })
        this.setState({ loading: false })
      })
  }

  resetPassword = () => {
    const { form } = this.state
    const errors = []
    let errorMessages = []
    if (form.password.length < 6) {
      errors.push('password')
      errorMessages.push('Password Minimal 6 Karakter')
    }
    if (form.password !== form.password_confirmation) {
      errors.push('password_confirmation')
      errorMessages.push('Password Konfirmasi Tidak Cocok')
    }
    this.setState({ errors })
    if (errorMessages.length > 0) {
      Toast.show({
        text: errorMessages[0],
        duration: 3000,
        buttonText: 'Ok'
      })
    } else {
      this.setState({ loading: true })
      fetch.post(resetPassword, form)
        .then(res => {
          console.log(res)
          this.setState({ loading: false })
          Toast.show({
            text: 'Password berhasil diperbaharui, silahkan login dengan password baru anda!',
            duration: 3000,
            buttonText: 'Ok'
          })
          const resetAction = StackActions.reset({
            index: 0,
            key: null,
            actions: [NavigationActions.navigate({ routeName: 'Login' })]
          })
          this.props.navigation.dispatch(resetAction)
        })
        .catch(err => {
          console.log(err, err.response)
          this.setState({ loading: false })
          Toast.show({
            text: err.response.data.msg,
            duration: 3000,
            buttonText: 'Ok'
          })
        })
    }
  }

  render() {
    let { form, errors } = this.state
    let disabled = false
    if (form.code === '' || form.password === '' || form.password_confirmation === '') {
      disabled = true
    }
    return (
      <Container style={{ backgroundColor: '#2196F3' }}>
        <View style={{ flex: 1, padding: 20 }}>
          <Spinner visible={this.state.loading} />
          <View style={{ paddingVertical: 30 }}>
            <Text style={{ color: '#fff', fontSize: 18, textAlign: 'center' }}>Silahkan Masukan kode verifikasi yang sudah kami kirimkan melalui SMS</Text>
          </View>
          <View style={{ flex: 1, margin: 20, marginTop: 30 }}>
            <Item error={errors.includes('code')}>
              <Input
                placeholder="Kode Verifikasi"
                value={form.code}
                onChangeText={code => this.setState({form: {...form, code}})}
                style={{ textAlign: 'center', color: '#fff', fontSize: 20 }}
                placeholderTextColor="#ffffff90"
              />
            </Item>
            <Item error={errors.includes('password')} style={{ marginVertical: 20 }}>
              <Input
                placeholder="Password Baru"
                value={form.password}
                onChangeText={password => this.setState({form: {...form, password}})}
                secureTextEntry={true}
                style={{ textAlign: 'center', color: '#fff', fontSize: 20 }}
                placeholderTextColor="#ffffff90"
              />
            </Item>
            <Item error={Boolean(form.password !== '' && form.password !== form.password_confirmation)}>
              <Input
                placeholder="Ulangi Password"
                value={form.password_confirmation}
                onChangeText={password_confirmation => this.setState({form: {...form, password_confirmation}})}
                secureTextEntry={true}
                style={{ textAlign: 'center', color: '#fff', fontSize: 20 }}
                placeholderTextColor="#ffffff90"
              />
            </Item>
            <View style={{ marginTop: 35 }}>
              <Button light block onPress={this.resetPassword} disabled={disabled}>
                <Text>Ganti Password</Text>
              </Button>
            </View>
          </View>
        </View>
      </Container>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  setToken: bindActionCreators(setToken, dispatch)
})

export default connect(null, mapDispatchToProps)(VerifyForgetPassword)

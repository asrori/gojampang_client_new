import React, { Component } from 'react';
import { Image, View, Alert, Modal, ActivityIndicator } from 'react-native';
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Badge
} from 'native-base';
import styles from './style';
import fetch from '../../services/fetch'
import { logout as apiLogout } from '../../config/api'
import { clearToken } from "../../store/actions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { NavigationActions, StackActions } from 'react-navigation'

const drawerCover = require('../../assets/drawer2.png');

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4,
      routes: [
        { name: 'Pesanan Masuk', routeName: 'EntryOrder', icon: 'envelope' },
        { name: 'Pesanan Dikonfirmasi', routeName: 'ConfirmedOrder', icon: 'folder-open' },
        { name: 'Pesanan Ditolak', routeName: 'RejectedOrder', icon: 'times' },
        { name: 'Riwayat Pesanan', routeName: 'HistoryOrder', icon: 'history' },
        { name: 'Bantuan Go Jampang', routeName: 'Help', icon: 'question-circle' },
        { name: 'Perjanjian Kemintraan', routeName: 'Agreement', icon: 'list-alt' }
      ],
      loading: false
    };
  }

  logout () {
    this.props.navigation.toggleDrawer()
    Alert.alert(
      'Logout',
      'Anda ingin keluar?',
      [
        { text: 'Tidak' },
        { text: 'Ya', onPress: () => this.doLogout() }
      ]
    )
  }

  doLogout() {
    this.setState({
      loading: true
    }, () => {
      fetch.post(apiLogout)
        .then(response => {
          this.setState({
            loading: false
          })
          this.resetLogin()
        })
        .catch(error => {
          console.log({ error })
          this.resetLogin()
        })
    })
  }

  resetLogin () {
    this.props.clearToken()
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        key: null,
        actions: [NavigationActions.navigate({ routeName: 'Login' })]
      })
    )
  }

  render() {
    const { loading } = this.state
    return (
      <Container>
        <Modal onRequestClose={() => null} visible={loading}>
          <View style={{ flex: 1, backgroundColor: '#dcdcdc', alignItems: 'center', justifyContent: 'center' }}>
            <View style={{ borderRadius: 10, backgroundColor: 'white', padding: 25 }}>
              <Text style={{ fontSize: 20, fontWeight: '200' }}>Loging Out...</Text>
              <ActivityIndicator size="large" />
            </View>
          </View>
        </Modal>
        <Content
          bounces={false}
          style={{ flex: 1, backgroundColor: '#fff', top: -1 }}
        >
          <Image source={drawerCover} style={styles.drawerCover} />
          <View style={styles.drawerImage}>
            <Text style={{color: '#fff'}}>{this.props.account.name.toUpperCase()}</Text>
            {/* <Text style={{color: '#fff'}}>{this.props.account.email}</Text> */}
            <Text style={{color: '#fff'}}>{this.props.account.address}</Text>
          </View>

          <List>
          {this.state.routes.map((route, i) => {
            return (
              <ListItem
                key={i}
                button
                noBorder
                onPress={() => this.props.navigation.navigate(route.routeName)}
              >
                <Left>
                  <Icon
                    active
                    type='FontAwesome'
                    name={route.icon}
                    style={{ color: '#777', fontSize: 17, width: 30 }}
                  />
                  <Text style={{ fontSize: 14 }}>
                    {route.name}
                  </Text>
                </Left>
              </ListItem>
            )}
          )}
            <ListItem
              button
              noBorder
              onPress={() => this.logout()}
            >
              <Left>
                <Icon
                  active
                  type='FontAwesome'
                  name={'sign-out'}
                  style={{ color: '#777', fontSize: 17, width: 30 }}
                />
                <Text style={{ fontSize: 14 }}>
                  Keluar
                </Text>
              </Left>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  account: state.accountInfo
})

const mapDispatchToProps = (dispatch) => ({
  clearToken: bindActionCreators(clearToken, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(SideBar)

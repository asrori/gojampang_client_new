import axios from 'axios'
import Config from 'react-native-config'

let headers = {
  'Content-Type': 'application/json'
}

export default axios.create({
  baseURL: Config.HOST,
  headers
})
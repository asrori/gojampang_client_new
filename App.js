/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { Root } from 'native-base'
import Navigator from './src/Navigator'
import fetch from './src/services/fetch'
import { Provider } from 'react-redux'
import { store } from './src/store'
import { persistStore } from 'redux-persist'

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      store: null
    }
  }

  componentWillMount() {
    persistStore(store, null, () => {
      console.log('rehydration is complete', store)
      this.setState({ store })
    })
  }

  render() {
    let initialRouteName = 'Login'
    if (this.state.store === null) {
      return null
    } else {
      const state = this.state.store.getState()
      if (state.token.access_token) {
        fetch.defaults.headers.common['Authorization'] = `Bearer ${state.token.access_token}`
        initialRouteName = 'MainScreen'
      }
    }
    let Scenes = Navigator(initialRouteName)
    return (
      <Provider store={this.state.store}>
        <Root>
          <Scenes />
        </Root>
      </Provider>
    );
  }
}
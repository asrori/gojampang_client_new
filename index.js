XMLHttpRequest = GLOBAL.originalXMLHttpRequest ? GLOBAL.originalXMLHttpRequest : GLOBAL.XMLHttpRequest;
import { AppRegistry } from 'react-native';
import App from './App';

AppRegistry.registerComponent('gojampang_client', () => App);
